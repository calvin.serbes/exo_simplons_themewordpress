<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <link rel="stylesheet" href="/leword/wp-content/themes/montheme/style.css">
</head>
<body>
    <div class="beast">
        <div class="title_beast">
            <h2 style="color: #fff;">BESTIAIRE</h2>
            <?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
                <h3 style="color: #fff;">
                    <?php echo get_field('nom'); ?>
                </h3>
            <?php endwhile; endif; ?>

        </div>
    </div>

    <?php
        // the query
        $the_query = new WP_Query( $args ); ?>

        <?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>

            <?php $image = get_field('image'); ?>

            <div class="animal">
                <div class="image" style="background: url('<?php echo get_field('image')['url']; ?>'); background-size: cover;";></div>
                <div class="infos">
                    <p class="name">
                        <?php echo get_field('nom'); ?>
                    </p>
                    <p class="description">
                        <?php echo get_field('description'); ?>
                    </p>
                </div>
            </div>
            
            <?php $mange = get_field('mange'); ?>

        <?php endwhile; endif; ?>

        <?php if(!empty($mange)){ ?>
        
        <div class="manger">
                
            <?php for($i = 0; $i < count($mange); $i++){?>
                
                <a href="<?php echo $mange[$i]->guid; ?>" class="labouffe">
                    <div class="test" style="background: url('<?php echo get_field('image', $mange[$i]->ID)['url']; ?>'); background-size: cover;" ></div> 
                    <p><?php echo get_field('mange')[$i]->post_title; ?></p>
                </a>    
                
            <?php }; ?>

        </div>

        <?php } ?>
           
            
</body>
</html>