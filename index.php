<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Le Bestiaire</title>
    <link rel="stylesheet" href="/leword/wp-content/themes/montheme/style.css">
</head>
<body>

    <header class="present">
        <h1 style="color: #fff;">BESTIAIRE</h1>
    </header>

    <div class="index">
        <div class="title">
            <h2 style="color: rgb(80, 80, 80);">INDEX</h2>
        </div>

        <?php
        // the query
        $the_query = new WP_Query( $args ); ?>


    <div class="beast_container">

        <?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>

            <a href="<?php echo get_permalink()?>" class="lesanimaux">
                <div class="beast_card">
                    <div class="beast_img" style="background: url('<?php echo get_field('image')['url']; ?>'); background-size: cover;";></div>
                    <h3 class="name">
                        <?php echo get_field('nom'); ?>
                    </h3>
                </div>
            </a>

        <?php endwhile; endif; ?>

        </div>

    </div>

    <?php wp_footer(); ?>

    <?php include "/leword/wp-content/themes/montheme/footer.php"; ?>

</body>
</html>